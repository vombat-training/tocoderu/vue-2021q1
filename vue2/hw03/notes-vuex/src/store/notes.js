export default {
  state: {
    notes: [
      {
        title: "Первая заметка",
        description: "содержание заметки",
        date: new Date(Date.now()).toLocaleString(),
        status: 1,
      },
      {
        title: "Вторая заметка",
        description: "содержание заметки",
        date: new Date(Date.now()).toLocaleString(),
        status: 0,
      },
      {
        title: "Третья заметка",
        description: "содержание заметки",
        date: new Date(Date.now()).toLocaleString(),
        status: 1,
      },
      {
        title: "Четвертая заметка",
        description: "содержание заметки",
        date: new Date(Date.now()).toLocaleString(),
        status: 2,
      },
    ],
  },
  mutations: {
    setInitial(state, data) {
      state.notes.push(...data);
    },
  },
  actions: {
    setInitial({ commit }, payload) {
      commit("setInitial", payload);
    },
  },
  getters: {
    getNotes(state) {
      return state.notes;
    },
  },
};
