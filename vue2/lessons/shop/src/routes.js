import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

import HomePage from '@/pages/Home.vue'
import NotFoundPage from '@/pages/404.vue'
import ShopPage from '@/pages/Shop.vue'
import ProductPage from '@/pages/Product.vue'

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomePage
        },
        {
            path: '/shop',
            name: 'shop',
            component: ShopPage
        },
        {
            path: '/shop/:id',
            name: 'product',
            component: ProductPage
        },
        {
            path: '*',
            name: 'notFoundPage',
            component: NotFoundPage
        },

    ]
})