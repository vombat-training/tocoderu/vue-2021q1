import { SET_MAIN_STEP } from "@/constants/constants.js";

export default {
  state: {
    messages: [],
  },
  mutations: {
    setMessage(state, payload) {
      state.messages = payload;
    },
  },
  actions: {
    setMessage({ commit }, payload) {
      commit("setMessage", payload);
    },
    loadMessages({ getters }) {
        getters.getMessageFilter.forEach((item) => (item.main = true))
    },
  },
  getters: {
    getMessage(state) {
      return state.messages;
    },
    getMessageMain(state) {
      return state.messages.filter((message) => message.main == true);
    },
    getMessageFilter(state) {
      return state.messages
        .filter((message) => message.main == false)
        .filter((item, index) => index < SET_MAIN_STEP);
    },
  },
};
