import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import about from './about'
export default new Vuex.Store({
    modules: {
        about,
    }
})

