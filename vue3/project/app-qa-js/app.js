const dataFile = 'qa.json'
const dataQ = [];

// считываем данные из json-файла
fetch(dataFile)
  .then(response => response.json())
  .then(json => initialize(json))

const current = {
  q: 0, // номер текущего вопроса
  a: 0  // номер ответа
}

const app = document.getElementById('app');
const divContainer = document.createElement('div');
const divWrapper = document.createElement('div');

const initialize = (data) => {
  dataQ.push(...data)

  const styleDivContainer = 'width: 650px; margin: 0 auto; border: 1px solid cyan'
  divContainer.style.cssText = styleDivContainer;
  divContainer.id = 'container';

  app.appendChild(divContainer);

  const styleDivWrapper = 'text-align: left; padding-left: 15px; padding-bottom: 20px'
  divWrapper.style.cssText = styleDivWrapper;
  divWrapper.id = 'wrapper'
  divContainer.appendChild(divWrapper);

  render();
}

const pager = () => {
  const pager = document.createElement('div')
  const buttonPrev = document.createElement('button');
  const buttonNext = document.createElement('button');

  pager.id = 'pager';
  buttonPrev.id = 'prev';
  buttonPrev.innerText = '<<';
  buttonNext.id = 'next';
  buttonNext.innerText = '>>';

  pager.style.cssText = 'margin-top: 20px';
  buttonNext.style.cssText = 'margin-left: 20px';

  buttonPrev.disabled = current.q == 0 ? true : false;
  buttonNext.disabled = current.q == dataQ.length - 1 ? true : false;

  buttonPrev.addEventListener("click", onClick, false);
  buttonNext.addEventListener("click", onClick, false);

  pager.appendChild(buttonPrev);
  pager.appendChild(buttonNext);

  document.getElementById('wrapper').appendChild(pager);
}
const render = () => {
  const title = document.createElement('h2');
  const subtitle = document.createElement('h3');
  const descr = document.createElement('p');

  title.id = 'title'
  subtitle.id = 'subtitle'
  descr.id = 'descr'

  title.innerHTML = 'Вопрос ' + dataQ[current.q].id + " из " + dataQ.length;
  subtitle.innerHTML = dataQ[current.q].title;
  descr.innerHTML = dataQ[current.q].descr;

  divWrapper.appendChild(title)
  divWrapper.appendChild(subtitle)
  divWrapper.appendChild(descr)

  dataQ[current.q].options.forEach(item => {
    const div = document.createElement('div');

    const input = document.createElement('input');
    input.type = 'radio';
    input.id = item.id;
    input.value = item.title;
    input.name = dataQ[current.q].id;
    input.addEventListener("change", onChange, false);

    const label = document.createElement('label');
    label.for = item.id;
    label.innerHTML = `№${item.id} -- '${item.title}'`;

    div.appendChild(input)
    div.appendChild(label)
    document.getElementById('wrapper').appendChild(div)
  });

  pager();
}

const onChange = (event) => {
  current.a = event.target.id;
  console.log(
    `Ваш выбор: Вопрос ${dataQ[current.q].id}, Ответ ${current.a}`
  );
}
const onClick = (event) => {
  if (event.target.id == "next" && current.q < dataQ.length - 1) {
    current.q = current.q + 1;
  }
  if (event.target.id == "prev" && current.q > 0) {
    current.q = current.q - 1;
  }
  current.a = 0;

  while (divWrapper.hasChildNodes()) {
    divWrapper.removeChild(divWrapper.firstChild);
  }
  render();
}

